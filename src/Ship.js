var Ship = BaseScreen.extend({
    imageShip: null,
    imageType: null,
    labelType: null,
    imageBounding1: null,
    imageBounding2: null,

    _effect: null,
    _type: 0,
    ctor: function() {
        this._super();
        this.syncAllChild("minigame/Ship.json");

        this._effect = AnimationMgr.runSpineAnimation(AniSpineSource.qb_warship_2, this.imageShip, cc.p(this.imageShip.width / 2, this.imageShip.height / 2), "normal", true);
        this._effect.setScale(0.7);

        this.imageShip.setCascadeOpacityEnabled(false);
        this.imageShip.setOpacity(0);
        return;
    },
    receiveHit: function(boom) {
        var pos = boom.getParent().convertToWorldSpace(boom.getPosition());
        cc.log(pos.toSource());
        pos = this.imageShip.convertToNodeSpace(pos);
        cc.log(pos.toSource());
        AnimationMgr.runSpineAnimation(AniSpineSource.qb_ship_take_damage, this.imageShip, pos, "hit_ship2", false);

        this._effect.setAnimation(0, "normal_get_hit", false);
        var onFinishAnimations = function () {
            this._effect.setAnimation(0, "normal", true);
        }.bind(this);
        this._effect.setCompleteListener(onFinishAnimations);
    },
    setType: function(type) {
        this._type = type;
        if (type % 2 == 0) {
            this.labelType.setString("EVEN");
        } else {
            this.labelType.setString("ODD");
        }
    },
    isCollisionBoom: function(boom) {
        var posBoom = boom.getBoundingBoxToWorld();
        var posBounding1 = this.imageBounding1.getBoundingBoxToWorld();
        //var posBounding2 = this.imageBounding2.getBoundingBoxToWorld();
        if (cc.rectIntersectsRect(posBoom, posBounding1)/* || cc.rectIntersectsRect(posBoom, posBounding2)*/) {
            return true;
        }
        return false;
    }
});