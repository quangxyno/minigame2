var Utility = {};
Utility.numberToStr = function(number){
    if(number == null) return "0";
    if (number.toString().indexOf(".") != -1) {
        return number.toString();
    }
    var _point = ".";

    var strExp = "";
    var strExpOther = number.toString();
    var isNegative = false;
    if (number < 0)
    {
        isNegative = true;
        strExpOther = strExpOther.slice(1, strExpOther.length);
    }
    var count = Math.floor(strExpOther.length / 3);
    for (var i = count; i > 0; i--)
    {
        var s = strExpOther.substr(strExpOther.length - 3, 3);
        if (i == 1 && (strExpOther.length % 3 == 0))
            strExp = s + strExp;
        else
            strExp = _point + s + strExp;
        strExpOther = strExpOther.substr(0, strExpOther.length-3);
    }
    strExp = strExpOther + strExp;
    if (isNegative)
    {
        strExp = "-" + strExp;
    }
    return strExp;
};