var AnimationMgr = {};
AnimationMgr.FOLDER_PATH = "res/animation/";

var AniSpineSource = {
    qb_ship_fire: "qb_ship_fire",
    qb_ship_take_damage: "qb_ship_take_damage",
    qb_warship_2: "qb_warship_2"
};

var AniPathFolder = {};
AnimationMgr.createSpineAnimation = function(key) {
    var jsonName = key + ".json";
    var atlasName = key + ".atlas";

    var folderPath = AnimationMgr.FOLDER_PATH;
    if (AniPathFolder[key] != null) {
        folderPath = AniPathFolder[key];
    }

    cc.log(folderPath + jsonName);
    cc.log(folderPath + atlasName);
    var animO = new sp.SkeletonAnimation(
        folderPath + jsonName,
        folderPath + atlasName
    );

    animO.retain();
    return animO;
};


AnimationMgr.runSpineAnimation = function(key, parent, pos, aniName, isLoop, callback   , isKeep ) {

    if (isKeep == undefined || isKeep == null) isKeep = false;

    var animation = AnimationMgr.createSpineAnimation(key);

    animation.setPosition(pos);
    parent.addChild(animation);
    animation.setAnimation(0, aniName, isLoop);     //run

    animation.onFinishAnimations = function () {
        if (!isLoop){

            if (!isKeep){
                if (animation.parent != null) {
                    animation.stopAllActions();
                    animation.runAction(cc.sequence(cc.spawn(cc.fadeOut(0.4), cc.scaleTo(0.4, 0.9).easing(cc.easeBackIn()) ), cc.callFunc(function () {

                        //animation.parent.removeChild(animation);
                    }.bind(this) )) );

                    //animation.parent.removeChild(animation);
                    setTimeout(function () {
                        animation.parent.removeChild(animation);
                    }.bind(this), 400)
                }
            }

            if (callback != undefined && callback != null) callback();
        }
    };
    animation.setCompleteListener(animation.onFinishAnimations.bind(animation));
    return animation;
};