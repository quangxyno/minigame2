var BaseScreen = cc.Layer.extend({
    screenConfig:null,
    fog:null,
    _clickEnable:true,
    _tintDark : null,
    _changeLocalize:true,
    _currId:"",
    _enableKeyboardListener:true,
    _isShowing:false,

    ctor:function(){
        this._super();
        this._tintDark = cc.tintTo(0.5, 140, 140, 140);
        this._tintDark.retain();

        return true;
    },

    syncAllChild:function(res) {
        this._currId = res;
        var path = "res/";
        this.screenConfig = ccs.load(path + res);
        this._rootNode = this.screenConfig.node;
        var size = this._rootNode.getContentSize();
        var designSize =  cc.size(1280, 720);
        if(size.width >= designSize.width && size.height >= designSize.height){
            //xu ly da man hinh
            var visibleSize = cc.director.getVisibleSize();
            this._rootNode.setContentSize(visibleSize);
            ccui.Helper.doLayout(this._rootNode);
        }

        this._rootNode.setAnchorPoint(cc.p(0.5, 0.5));
        this._rootNode.setPosition(cc.p(this._rootNode.width/2, this._rootNode.height/2));
        this.addChild(this._rootNode, 0);

        var allChildren = this._rootNode.getChildren();
        this.syncAllChildHelper(allChildren);
    },
    resyncAllChild:function(res){
        if(this._rootNode == null) return;
        var allChildren = this._rootNode.getChildren();
        for(var i = 0; i < allChildren.length; i++) {
            if(allChildren[i].parent != null) {
                allChildren[i].parent.removeChild(allChildren[i]);
            }
        }
        this.syncAllChild(res);
    },

    syncAllChildHelper:function(allChildren){
        if(allChildren.length == 0) return;
        var nameChild;
        for(var i = 0; i < allChildren.length; i++) {
            nameChild = allChildren[i].getName();
            if(nameChild == undefined) continue;
            var arr = nameChild.split("_");
            //if(arr.length != 2) continue;
            if(arr.length > 2) {
                this[nameChild] = allChildren[i];
                continue;
            }
            nameChild = arr[0] + arr[1];
            if(nameChild in this)
            {
                this[nameChild] = allChildren[i];
                //Utility.convertPosByResolutionSize(allChildren[i].x);
                if(arr[0] == "btn")
                {
                    //this[nameChild].setPressedActionEnabled(true);
                    this[nameChild].addTouchEventListener(this.onTouchEvent, this);
                }
                this.syncAllChildHelper(this[nameChild].getChildren());
            }
        }
    },

    convertAlignCustomRichText:function(alignHorizontal, alignVertical){
        switch (alignHorizontal){
            case cc.TEXT_ALIGNMENT_CENTER:
                alignHorizontal = RichTextAlignment.CENTER;
                break;
            case cc.TEXT_ALIGNMENT_RIGHT:
                alignHorizontal = RichTextAlignment.RIGHT;
                break;
            case cc.TEXT_ALIGNMENT_LEFT:
                alignHorizontal = RichTextAlignment.LEFT;
                break;

        }
        switch (alignVertical){
            case cc.VERTICAL_TEXT_ALIGNMENT_TOP:
                alignVertical = RichTextAlignment.TOP;
                break;
            case cc.VERTICAL_TEXT_ALIGNMENT_CENTER:
                alignVertical = RichTextAlignment.MIDDLE;
                break;
            case cc.VERTICAL_TEXT_ALIGNMENT_BOTTOM:
                alignVertical = RichTextAlignment.BOTTOM;
                break;
        }
        return cc.p(alignHorizontal, alignVertical);
    },

    createFog:function(alpha, enableTouch){
        this.fog = new ccui.Layout();
        this.fog.setBackGroundColorType(ccui.Layout.BG_COLOR_SOLID);
        this.fog.setBackGroundColor(cc.color.BLACK);
        this.fog.setContentSize(cc.size(cc.winSize.width, cc.winSize.height));
        this.fog.setPosition(cc.p(0, 0));
        if(alpha == null) this.fog.setOpacity(255/100*50);
        else this.fog.setOpacity(255/100*alpha);
        if(enableTouch == null) this.fog.setTouchEnabled(true);
        else this.fog.setTouchEnabled(enableTouch);
    },

    showDisable:function(alpha, enableTouch){
        if(this.fog != null && this.fog.parent != null) {
            this.removeChild(this.fog);
            this.fog = null;
        }
        this.createFog(alpha, enableTouch);
        this.addChild(this.fog, -1);
    },

    hideDisable:function(){
        if(this.fog == null) return;
        this.removeChild(this.fog);
        this.fog = null;
    },

    onTouchEvent:function(sender, type){
        switch (type){
            case ccui.Widget.TOUCH_BEGAN:
                this.onTouchBeganEvent(sender);
                break;
            case ccui.Widget.TOUCH_ENDED:
                this.onTouchEndEvent(sender);
                break;
            case ccui.Widget.TOUCH_CANCELED:
                this.onTouchCancelledEvent(sender);
                break;
            case ccui.Widget.TOUCH_MOVED:
                this.onTouchMovedEvent(sender);
                break;
        }
    },

    onTouchBeganEvent:function(sender){
        sender.stopAllActions();
        sender.runAction(cc.sequence(this._tintDark));
    },

    onTouchEndEvent:function(sender){
        sender.stopAllActions();
        sender.setColor(cc.color(255, 255, 255));
    },

    onTouchCancelledEvent:function(sender){
        sender.playedSound = false;
        sender.stopAllActions();
        sender.setColor(cc.color(255, 255, 255));
    },

    onTouchMovedEvent:function(sender){
    },

    showGui:function(){
        this._isShowing = true;
        if(this._changeLocalize) {
            this.localize();
        }
    },

    hideGui:function(){
        this._isShowing = false;
    },
});