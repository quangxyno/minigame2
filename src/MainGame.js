var MainGame = BaseScreen.extend({
    labelBtnNormal: null,
    btnNormal: null,
    imageLogo: null,
    btnRule: null,
    labelBtnHard: null,
    btnHard: null,
    imageLobby: null,
    labelPoint: null,
    imagePointBgr: null,
    progressBar: null,
    imageProgress: null,
    btnHome: null,
    imagePlay: null,
    imageControl: null,

    panelRule: null,
    imageRule: null,
    btnCloseRule: null,

    panelGameOver: null,
    imageGameOver: null,
    btnOverHome: null,
    btnRestart: null,
    labelScoreNum: null,
    labelHighNum: null,

    panelRed: null,


    _point: 0,
    _mode: 0,
    _curHP: 0,

    _curFly: null,

    _curBoom: null,
    listBoom: null,

    listShip: null,

    MODE_NORMAL: 0,
    MODE_HARD: 1,
    MAX_HP: 5,
    ctor: function() {
        this._super();
        this.syncAllChild("minigame/GameLayer.json");
        cc.audioEngine.playMusic("res/sound/home.mp3", true);

        this.imageControl.setCascadeOpacityEnabled(false);
        this.imageControl.setOpacity(0);

        this.listBoom = [];
        this.listShip = [];
        //cc.sys.localStorage.getItem(key);
        this.initShip();

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onDropBoom.bind(this)
        }, this);

        this.retain();
        return true;
    },
    update: function(dt) {
        if (this._curBoom && cc.sys.isObjectValid(this._curBoom) && !this._curBoom.isDrop && this._curFly && !this._curFly.dropBoom && cc.sys.isObjectValid(this._curFly)) {
            this._curBoom.x = this._curFly.x;
            this._curBoom.y = this._curFly.y - 40;
        }
        this.checkCollision();
    },
    checkCollision: function() {
        for (var i = 0; i < this.listBoom.length; i++) {
            for (var j = 0; j < this.listShip.length; j++) {
                if (this.listShip[j].isCollisionBoom(this.listBoom[i])) {
                    if (this.listBoom[i].result % 2 == this.listShip[j]._type) {
                        this.listShip[j].receiveHit(this.listBoom[i]);
                        this._point++;
                        this.labelPoint.setString(Utility.numberToStr(this._point));
                    } else {
                        this.minusHP();
                    }
                    this.listBoom[i].removeFromParent();
                    this.removeBoom(this.listBoom[i]);
                    return;
                }
            }
        }
    },
    initShip: function() {
        var shipOdd = new Ship();
        shipOdd.setType(1);
        this.imagePlay.addChild(shipOdd);
        shipOdd.setPosition(cc.p(cc.winSize.width / 4, 150));
        this.listShip.push(shipOdd);
        shipOdd.runAction(cc.sequence(cc.moveBy(1, -50, 0), cc.moveBy(2, 100, 0), cc.moveBy(1, -50, 0)).repeatForever());

        var shipEven = new Ship();
        shipEven.setType(0);
        this.imagePlay.addChild(shipEven);
        shipEven.setPosition(cc.p(cc.winSize.width - cc.winSize.width / 4, 150));
        shipEven.runAction(cc.sequence(cc.moveBy(1, -50, 0), cc.moveBy(2, 100, 0), cc.moveBy(1, -50, 0)).repeatForever());
        this.listShip.push(shipEven);

        this.panelGameOver.setLocalZOrder(10);
    },
    createFly: function() {
        var fly = new cc.Sprite("res/minigame/game_fly.png");
        this.imageControl.addChild(fly);
        var isReverse = Math.random() > 0.5? true : false;
        fly.dropBoom = false;
        if (isReverse) {
            fly.setPosition(cc.p(cc.winSize.width, cc.winSize.height - 100 - Math.random() * 50));
            fly.runAction(cc.sequence(cc.moveBy(10, -cc.winSize.width - 300, 0), cc.callFunc(function(){
                fly.removeFromParent();
                if (!fly.dropBoom) {
                    this.minusHP();
                    this.createFly();
                }
            }, this)));
            fly.setScale(0.6);
        } else {
            fly.setScale(-0.6, 0.6);
            fly.setPosition(cc.p(0, cc.winSize.height - 100 - Math.random() * 50));
            fly.runAction(cc.sequence(cc.moveBy(10, cc.winSize.width + 300, 0), cc.callFunc(function(){
                fly.removeFromParent();
                if (!fly.dropBoom) {
                    this.minusHP();
                    this.createFly();
                }
            }, this)));
        }
        this._curFly = fly;

        var boom = new cc.Sprite("res/minigame/game_boom.png");
        this.imageControl.addChild(boom);
        boom.setPosition(cc.p(120, 0));
        boom.isDrop = false;
        this._curBoom = boom;
        boom.setScaleX(isReverse? 0.6 : -0.6);
        boom.setScaleY(0.6);

        var label = new ccui.Text("", "res/minigame/font_default_bold.ttf", 32);
        label.enableOutline(cc.color(0, 0, 0), 2);
        boom.addChild(label);
        label.setPosition(boom.width / 2, -10);
        label.setScaleX(isReverse? 1 : -1);

        var num1 = 0;
        var num2 = 0;
        if (this._mode == this.MODE_NORMAL) {
            num1 = Math.round(Math.random() * 100);
            num2 = Math.round(Math.random() * 100);
        } else {
            num1 = Math.round(Math.random() * 1000);
            num2 = Math.round(Math.random() * 1000);
        }

        if (Math.random() > 0.5) {
            var result = num1 + num2;
            label.setString(num1 + " + " + num2);
            boom.result = result;
        } else {
            var result = num1 - num2;
            label.setString(num1 + " - " + num2);
            boom.result = result;
        }
    },
    showLobby: function() {
        this.unscheduleUpdate();
        this.imageLobby.setVisible(true);
        this.imagePlay.setVisible(false);
    },
    showGuiPlay: function(mode) {
        this._point = 0;
        this.labelPoint.setString(Utility.numberToStr(this._point));

        this._curHP = this.MAX_HP;
        this.setHP();
        this._mode = mode;
        this.imageLobby.setVisible(false);
        this.imagePlay.setVisible(true);
        this.panelGameOver.setVisible(false);

        this.imageControl.removeAllChildren();
        this._curBoom = null;
        this._curFly = null;
        this.listBoom = [];

        this.scheduleUpdate();
        this.createFly();
    },
    setHP: function() {
        this.progressBar.setPercent(this._curHP / this.MAX_HP * 100);
        this.progressBar.setColor(cc.color(255, 255 * this._curHP / this.MAX_HP, 0));
    },
    onDropBoom: function() {
        if (this._curBoom) {
            var boom = this._curBoom;
            boom.isDrop = true;
            boom.runAction(cc.sequence(cc.moveBy(5, 0, -cc.winSize.height + 120), cc.callFunc(function() {
                this.minusHP();
                this.removeBoom(boom);
            }, this)));
            this.addBoomFalling(boom);
        }
        if (this._curFly) {
            this._curFly.dropBoom = true;
            this.createFly();
        }
        return true;
    },
    addBoomFalling: function(obj) {
        this.listBoom.push(obj);
    },
    removeBoom: function(obj) {
        for (var i = 0; i < this.listBoom.length; i++) {
            if (this.listBoom[i] === obj) {
                this.listBoom.splice(i, 1);
                break;
            }
        }
    },
    minusHP: function() {
        this._curHP--;
        this.setHP();
        if (this._curHP <= 0) {
            this.showGameOver();
        }
        this.panelRed.runAction(cc.sequence(cc.fadeTo(0.2, 150), cc.fadeOut(0.2)).repeat(3));
    },
    showGameOver: function() {
        this.panelGameOver.setVisible(true);
        this.labelScoreNum.setString(Utility.numberToStr(this._point));

        var highScore = cc.sys.localStorage.getItem("high_score") || 0;
        this.labelHighNum.setString(Utility.numberToStr(highScore));
        if (this._point > highScore) {
            cc.sys.localStorage.setItem("high_score", this._point);
        }
        this.imageControl.removeAllChildren();
        this._curBoom = null;
        this._curFly = null;
        this.listBoom = [];
        this.unscheduleUpdate();
    },
    onTouchEndEvent:function(sender){
        this._super(sender);
        switch (sender) {
            case this.btnHome:
            case this.btnOverHome:
                this.showLobby();
                break;
            case this.btnNormal:
                this.showGuiPlay(this.MODE_NORMAL);
                break;
            case this.btnHard:
                this.showGuiPlay(this.MODE_HARD);
                break;
            case this.btnCloseRule:
                this.panelRule.setVisible(false);
                break;
            case this.btnRule:
                this.panelRule.setVisible(true);
                break;
            case this.btnRestart:
                this.showGuiPlay(this._mode);
                break;
        }
    }
});