<GameFile>
  <PropertyGroup Name="GameLayer" Type="Scene" ID="4e1eee2f-b2b6-430a-8f35-db5ce4510d60" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="85" ctype="GameNodeObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="image_Lobby" ActionTag="137743707" Tag="86" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TouchEnable="True" StretchWidthEnable="True" StretchHeightEnable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="btn_Normal" ActionTag="2001410903" Tag="89" IconVisible="False" HorizontalEdge="BothEdge" LeftMargin="263.0485" RightMargin="730.9515" TopMargin="478.5514" BottomMargin="106.4486" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="256" Scale9Height="113" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="286.0000" Y="135.0000" />
                <Children>
                  <AbstractNodeData Name="label_BtnNormal" ActionTag="1166129639" Tag="90" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="61.0000" RightMargin="61.0000" TopMargin="31.4999" BottomMargin="43.5001" FontSize="36" LabelText="NORMAL" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="164.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="143.0000" Y="73.5001" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5444" />
                    <PreSize X="0.5734" Y="0.4444" />
                    <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                    <OutlineColor A="255" R="40" G="75" B="2" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="406.0485" Y="173.9486" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3172" Y="0.2416" />
                <PreSize X="0.2234" Y="0.1875" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_green_btn.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_green_btn.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_green_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="image_Logo" ActionTag="1216564993" Tag="88" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="161.5000" RightMargin="161.5000" TopMargin="105.0776" BottomMargin="279.9224" LeftEage="315" RightEage="315" TopEage="110" BottomEage="110" Scale9OriginX="315" Scale9OriginY="110" Scale9Width="327" Scale9Height="115" ctype="ImageViewObjectData">
                <Size X="957.0000" Y="335.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="640.0000" Y="447.4224" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6214" />
                <PreSize X="0.7477" Y="0.4653" />
                <FileData Type="Normal" Path="game_logo_img.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_Rule" ActionTag="-716410674" Tag="87" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="1139.7241" RightMargin="25.2759" TopMargin="37.2922" BottomMargin="566.7078" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="85" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="115.0000" Y="116.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1197.2241" Y="624.7078" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9353" Y="0.8676" />
                <PreSize X="0.0898" Y="0.1611" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_rule_btn.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_rule_btn.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_rule_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_Hard" ActionTag="-2062000532" Tag="91" IconVisible="False" HorizontalEdge="BothEdge" LeftMargin="726.8989" RightMargin="267.1011" TopMargin="478.5514" BottomMargin="106.4486" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="256" Scale9Height="113" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="286.0000" Y="135.0000" />
                <Children>
                  <AbstractNodeData Name="label_BtnHard" ActionTag="-1345221454" Tag="92" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="89.0000" RightMargin="89.0000" TopMargin="31.4999" BottomMargin="43.5001" FontSize="36" LabelText="HARD" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="108.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="143.0000" Y="73.5001" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5444" />
                    <PreSize X="0.3776" Y="0.4444" />
                    <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                    <OutlineColor A="255" R="134" G="57" B="4" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="869.8989" Y="173.9486" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6796" Y="0.2416" />
                <PreSize X="0.2234" Y="0.1875" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_orange_btn.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_orange_btn.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_orange_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="game_bgr.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="panel_Rule" ActionTag="-1119277311" VisibleForFrame="False" Tag="117" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="126" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="image_Rule" ActionTag="2024272592" Tag="118" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="332.0001" RightMargin="331.9999" TopMargin="142.5000" BottomMargin="142.5000" LeftEage="203" RightEage="203" TopEage="143" BottomEage="143" Scale9OriginX="203" Scale9OriginY="143" Scale9Width="210" Scale9Height="149" ctype="ImageViewObjectData">
                <Size X="616.0000" Y="435.0000" />
                <Children>
                  <AbstractNodeData Name="Image_10" ActionTag="-189671781" Tag="120" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="111.5000" RightMargin="111.5000" TopMargin="-33.1086" BottomMargin="338.1086" LeftEage="129" RightEage="129" TopEage="42" BottomEage="42" Scale9OriginX="129" Scale9OriginY="42" Scale9Width="135" Scale9Height="46" ctype="ImageViewObjectData">
                    <Size X="393.0000" Y="130.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="308.0000" Y="403.1086" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.9267" />
                    <PreSize X="0.6380" Y="0.2989" />
                    <FileData Type="Normal" Path="title_bgr.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="label_Rule" ActionTag="1776749960" Tag="121" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="222.0000" RightMargin="222.0000" TopMargin="-17.4346" BottomMargin="415.4346" FontSize="24" LabelText="HOW TO PLAY" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="172.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="308.0000" Y="433.9346" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="92" />
                    <PrePosition X="0.5000" Y="0.9976" />
                    <PreSize X="0.2792" Y="0.0851" />
                    <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_11" ActionTag="1846389997" Tag="122" IconVisible="False" LeftMargin="93.9996" RightMargin="88.0004" TopMargin="146.0006" BottomMargin="180.9994" LeftEage="143" RightEage="143" TopEage="35" BottomEage="35" Scale9OriginX="143" Scale9OriginY="35" Scale9Width="148" Scale9Height="38" ctype="ImageViewObjectData">
                    <Size X="434.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="310.9996" Y="234.9994" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5049" Y="0.5402" />
                    <PreSize X="0.7045" Y="0.2483" />
                    <FileData Type="Normal" Path="img_rule.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btn_CloseRule" ActionTag="374850027" Tag="119" IconVisible="False" LeftMargin="544.9122" RightMargin="-10.9122" TopMargin="-12.5359" BottomMargin="364.5359" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="52" Scale9Height="61" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="82.0000" Y="83.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="585.9122" Y="406.0359" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9512" Y="0.9334" />
                    <PreSize X="0.1331" Y="0.1908" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Normal" Path="btn_Close.png" Plist="" />
                    <PressedFileData Type="Normal" Path="btn_Close.png" Plist="" />
                    <NormalFileData Type="Normal" Path="btn_Close.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="640.0001" Y="360.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4812" Y="0.6042" />
                <FileData Type="Normal" Path="board_bgr.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="image_Play" ActionTag="1844684472" VisibleForFrame="False" Tag="93" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" StretchWidthEnable="True" StretchHeightEnable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="image_Control" ActionTag="-219546672" Tag="138" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" Scale9Width="46" Scale9Height="46" ctype="ImageViewObjectData">
                <Size X="1280.0000" Y="720.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="640.0000" Y="360.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="image_PointBgr" ActionTag="-233668064" Tag="95" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="18.8556" RightMargin="1071.1444" TopMargin="14.6190" BottomMargin="654.3810" LeftEage="62" RightEage="62" TopEage="16" BottomEage="16" Scale9OriginX="62" Scale9OriginY="16" Scale9Width="66" Scale9Height="19" ctype="ImageViewObjectData">
                <Size X="190.0000" Y="51.0000" />
                <Children>
                  <AbstractNodeData Name="label_Point" ActionTag="1508013767" Tag="98" IconVisible="False" LeftMargin="32.6528" RightMargin="7.3472" TopMargin="8.3450" BottomMargin="2.6550" IsCustomSize="True" FontSize="30" LabelText="0" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="150.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="107.6528" Y="22.6550" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="103" />
                    <PrePosition X="0.5666" Y="0.4442" />
                    <PreSize X="0.7895" Y="0.7843" />
                    <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_5" ActionTag="-499582007" Tag="96" IconVisible="False" LeftMargin="1.2595" RightMargin="148.7405" TopMargin="7.7400" BottomMargin="-0.7400" LeftEage="13" RightEage="13" TopEage="14" BottomEage="14" Scale9OriginX="13" Scale9OriginY="14" Scale9Width="14" Scale9Height="16" ctype="ImageViewObjectData">
                    <Size X="40.0000" Y="44.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="21.2595" Y="21.2600" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1119" Y="0.4169" />
                    <PreSize X="0.2105" Y="0.8627" />
                    <FileData Type="Normal" Path="game_gold.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.8556" Y="679.8810" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0889" Y="0.9443" />
                <PreSize X="0.1484" Y="0.0708" />
                <FileData Type="Normal" Path="game_point_bgr.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="image_Progress" ActionTag="-1264597756" Tag="99" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="426.5000" RightMargin="426.5000" TopMargin="20.0000" BottomMargin="670.0000" LeftEage="140" RightEage="140" TopEage="9" BottomEage="9" Scale9OriginX="140" Scale9OriginY="9" Scale9Width="147" Scale9Height="12" ctype="ImageViewObjectData">
                <Size X="427.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="progress_Bar" ActionTag="689986034" Tag="100" IconVisible="False" LeftMargin="-0.7499" RightMargin="0.7499" TopMargin="1.0000" BottomMargin="-1.0000" ProgressInfo="100" ctype="LoadingBarObjectData">
                    <Size X="427.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="212.7501" Y="14.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="0" />
                    <PrePosition X="0.4982" Y="0.4667" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <ImageFileData Type="Normal" Path="game_progress_bar.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="640.0000" Y="685.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9514" />
                <PreSize X="0.3336" Y="0.0417" />
                <FileData Type="Normal" Path="game_progress_bgr.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_Home" ActionTag="-671342423" Tag="94" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="1150.9967" RightMargin="14.0033" TopMargin="16.2765" BottomMargin="587.7235" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="85" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="115.0000" Y="116.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1208.4967" Y="645.7235" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9441" Y="0.8968" />
                <PreSize X="0.0898" Y="0.1611" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="game_home_btn.png" Plist="" />
                <PressedFileData Type="Normal" Path="game_home_btn.png" Plist="" />
                <NormalFileData Type="Normal" Path="game_home_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="panel_GameOver" ActionTag="-800114980" VisibleForFrame="False" Tag="123" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
                <Size X="1280.0000" Y="720.0000" />
                <Children>
                  <AbstractNodeData Name="image_GameOver" ActionTag="359713139" Tag="124" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="332.0000" RightMargin="332.0000" TopMargin="142.5000" BottomMargin="142.5000" LeftEage="203" RightEage="203" TopEage="143" BottomEage="143" Scale9OriginX="203" Scale9OriginY="143" Scale9Width="210" Scale9Height="149" ctype="ImageViewObjectData">
                    <Size X="616.0000" Y="435.0000" />
                    <Children>
                      <AbstractNodeData Name="Image_13" ActionTag="826584094" Tag="125" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="111.4999" RightMargin="111.5001" TopMargin="-30.8872" BottomMargin="335.8872" LeftEage="129" RightEage="129" TopEage="42" BottomEage="42" Scale9OriginX="129" Scale9OriginY="42" Scale9Width="135" Scale9Height="46" ctype="ImageViewObjectData">
                        <Size X="393.0000" Y="130.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="307.9999" Y="400.8872" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.9216" />
                        <PreSize X="0.6380" Y="0.2989" />
                        <FileData Type="Normal" Path="title_bgr.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="label_Rule_0" ActionTag="696019111" Tag="127" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="233.5000" RightMargin="233.5000" TopMargin="-15.5573" BottomMargin="413.5573" FontSize="24" LabelText="GAME OVER" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="149.0000" Y="37.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="308.0000" Y="432.0573" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="0" G="0" B="92" />
                        <PrePosition X="0.5000" Y="0.9932" />
                        <PreSize X="0.2419" Y="0.0851" />
                        <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Text_7_0" ActionTag="-1433973078" Tag="132" IconVisible="False" LeftMargin="84.5712" RightMargin="436.4288" TopMargin="131.4774" BottomMargin="253.5226" FontSize="32" LabelText="Score" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="95.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="132.0712" Y="278.5226" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2144" Y="0.6403" />
                        <PreSize X="0.1542" Y="0.1149" />
                        <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                        <OutlineColor A="255" R="45" G="75" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_14" ActionTag="-1010370462" Tag="134" IconVisible="False" LeftMargin="201.5390" RightMargin="102.4610" TopMargin="125.3652" BottomMargin="240.6348" LeftEage="102" RightEage="102" TopEage="22" BottomEage="22" Scale9OriginX="102" Scale9OriginY="22" Scale9Width="108" Scale9Height="25" ctype="ImageViewObjectData">
                        <Size X="312.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="357.5390" Y="275.1348" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5804" Y="0.6325" />
                        <PreSize X="0.5065" Y="0.1586" />
                        <FileData Type="Normal" Path="img_point.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="btn_OverHome" ActionTag="-1983932109" Tag="128" IconVisible="False" LeftMargin="101.1077" RightMargin="337.8923" TopMargin="343.6622" BottomMargin="-17.6622" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="147" Scale9Height="87" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="177.0000" Y="109.0000" />
                        <Children>
                          <AbstractNodeData Name="Text_7" ActionTag="1921550050" Tag="129" IconVisible="False" LeftMargin="44.7443" RightMargin="43.2557" TopMargin="25.0292" BottomMargin="38.9708" FontSize="28" LabelText="HOME" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="89.0000" Y="45.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="89.2443" Y="61.4708" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.5042" Y="0.5640" />
                            <PreSize X="0.5028" Y="0.4128" />
                            <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                            <OutlineColor A="255" R="45" G="75" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="189.6077" Y="36.8378" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3078" Y="0.0847" />
                        <PreSize X="0.2873" Y="0.2506" />
                        <TextColor A="255" R="65" G="65" B="70" />
                        <DisabledFileData Type="Normal" Path="btn_green_1.png" Plist="" />
                        <PressedFileData Type="Normal" Path="btn_green_1.png" Plist="" />
                        <NormalFileData Type="Normal" Path="btn_green_1.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="btn_Restart" ActionTag="-1235065641" Tag="130" IconVisible="False" LeftMargin="309.1206" RightMargin="79.8794" TopMargin="340.6711" BottomMargin="-14.6711" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="197" Scale9Height="87" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="227.0000" Y="109.0000" />
                        <Children>
                          <AbstractNodeData Name="Text_7" ActionTag="-926724965" Tag="131" IconVisible="False" LeftMargin="56.2419" RightMargin="50.7581" TopMargin="25.0293" BottomMargin="38.9707" FontSize="28" LabelText="RESTART" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="120.0000" Y="45.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="116.2419" Y="61.4707" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.5121" Y="0.5640" />
                            <PreSize X="0.5286" Y="0.4128" />
                            <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                            <OutlineColor A="255" R="45" G="75" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="422.6206" Y="39.8289" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6861" Y="0.0916" />
                        <PreSize X="0.3685" Y="0.2506" />
                        <TextColor A="255" R="65" G="65" B="70" />
                        <DisabledFileData Type="Normal" Path="btn_green_2.png" Plist="" />
                        <PressedFileData Type="Normal" Path="btn_green_2.png" Plist="" />
                        <NormalFileData Type="Normal" Path="btn_green_2.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Text_7_0_0" ActionTag="-919300847" Tag="133" IconVisible="False" LeftMargin="85.5712" RightMargin="452.4288" TopMargin="211.6017" BottomMargin="171.3983" FontSize="32" LabelText="High" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="78.0000" Y="52.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="124.5712" Y="197.3983" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2022" Y="0.4538" />
                        <PreSize X="0.1266" Y="0.1195" />
                        <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                        <OutlineColor A="255" R="45" G="75" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Image_14_0" ActionTag="1030023091" Tag="135" IconVisible="False" LeftMargin="201.6014" RightMargin="102.3986" TopMargin="208.7512" BottomMargin="157.2488" LeftEage="102" RightEage="102" TopEage="22" BottomEage="22" Scale9OriginX="102" Scale9OriginY="22" Scale9Width="108" Scale9Height="25" ctype="ImageViewObjectData">
                        <Size X="312.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="357.6014" Y="191.7488" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5805" Y="0.4408" />
                        <PreSize X="0.5065" Y="0.1586" />
                        <FileData Type="Normal" Path="img_point.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="label_ScoreNum" ActionTag="1603908568" Tag="136" IconVisible="False" LeftMargin="349.7012" RightMargin="244.2988" TopMargin="134.5974" BottomMargin="250.4026" FontSize="32" LabelText="3" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="22.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="360.7012" Y="275.4026" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="0" />
                        <PrePosition X="0.5856" Y="0.6331" />
                        <PreSize X="0.0357" Y="0.1149" />
                        <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                        <OutlineColor A="255" R="45" G="75" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="label_HighNum" ActionTag="-1399470933" Tag="137" IconVisible="False" LeftMargin="351.2928" RightMargin="242.7072" TopMargin="221.0415" BottomMargin="163.9585" FontSize="32" LabelText="3" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="22.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="362.2928" Y="188.9585" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="0" />
                        <PrePosition X="0.5881" Y="0.4344" />
                        <PreSize X="0.0357" Y="0.1149" />
                        <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                        <OutlineColor A="255" R="45" G="75" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="640.0000" Y="360.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4812" Y="0.6042" />
                    <FileData Type="Normal" Path="board_bgr.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="640.0000" Y="360.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="game_play_bgr.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="panel_Red" ActionTag="-1699514206" Alpha="0" Tag="139" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="255" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>