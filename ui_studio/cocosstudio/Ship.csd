<GameFile>
  <PropertyGroup Name="Ship" Type="Node" ID="b99f3b44-30a0-416d-bf8b-b2168153d8d6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="110" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="image_Bounding2" ActionTag="-993787272" VisibleForFrame="False" Tag="111" IconVisible="False" LeftMargin="-52.9974" RightMargin="-27.0026" TopMargin="-83.2903" BottomMargin="3.2903" Scale9Width="46" Scale9Height="46" ctype="ImageViewObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-12.9974" Y="43.2903" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="image_Bounding1" ActionTag="653730126" VisibleForFrame="False" Tag="112" IconVisible="False" LeftMargin="-125.3837" RightMargin="-124.6163" TopMargin="5.2273" BottomMargin="-85.2273" Scale9Width="46" Scale9Height="46" ctype="ImageViewObjectData">
            <Size X="250.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.3837" Y="-45.2273" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="image_Ship" ActionTag="-1461049488" Tag="115" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-23.0000" RightMargin="-23.0000" TopMargin="-23.0000" BottomMargin="-23.0000" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="16" Scale9Height="16" ctype="ImageViewObjectData">
            <Size X="46.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="image_Type" ActionTag="-2134351281" Tag="113" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-88.0000" RightMargin="-88.0000" TopMargin="42.7346" BottomMargin="-93.7346" LeftEage="58" RightEage="58" TopEage="16" BottomEage="16" Scale9OriginX="58" Scale9OriginY="16" Scale9Width="60" Scale9Height="19" ctype="ImageViewObjectData">
            <Size X="176.0000" Y="51.0000" />
            <Children>
              <AbstractNodeData Name="label_Type" ActionTag="-1140586978" Tag="114" IconVisible="False" LeftMargin="8.2382" RightMargin="7.7618" TopMargin="6.3649" BottomMargin="4.6351" IsCustomSize="True" FontSize="28" LabelText="ODD" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="160.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="88.2382" Y="24.6351" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5014" Y="0.4830" />
                <PreSize X="0.9091" Y="0.7843" />
                <FontResource Type="Normal" Path="font_default_bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-68.2346" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_text_bgr.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>